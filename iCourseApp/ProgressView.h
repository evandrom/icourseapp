//
//  ProgressView.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 12/6/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

@property (weak, nonatomic) IBOutlet UIProgressView *Progress1;

@property (weak, nonatomic) IBOutlet UIProgressView *Progress2;

@property (weak, nonatomic) IBOutlet UIProgressView *Progress3;

@property (weak, nonatomic) IBOutlet UIProgressView *Progress4;

@property (weak, nonatomic) IBOutlet UIButton *Button1;

- (void) setupProgressView;
- (void) cancelProgress: (UIProgressView *) progress;
- (void) setProgress: (UIProgressView * ) progress withInterval:(CGFloat) interval;
- (void) resetAllProgress;


@end
