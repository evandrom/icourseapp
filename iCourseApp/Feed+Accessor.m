//
//  Feed+Accessor.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/10/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import "Feed+Accessor.h"
#import "AppDelegate.h"

@implementation Feed (Accessor)

- (instancetype) initFeedWithTitle: (NSString *) title andImage: (NSString *) image {
    if (self = [super init]) {
        self.titulo = title;
        self.image = image;
    }
    
    return self;
}

+ (NSString *) entityName {
    return @"Feed";
}

+ (instancetype)InsertNewObjectInManagedContext:(NSManagedObjectContext *)managedObjectContext {

    return [NSEntityDescription insertNewObjectForEntityForName:[Feed entityName] inManagedObjectContext:managedObjectContext];
    
}

+ (NSArray *)retrieveFeedsFromPerson:(Person *)person {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:[Feed entityName] inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    // Specify criteria for filtering which objects to fetch
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"whoPost == %@", person];
    [fetchRequest setPredicate:predicate];
    // Specify how the fetched objects should be sorted
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"titulo"
                                                                   ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects == nil) {
        NSLog(@"%@",error.description);
    }
    
    return fetchedObjects;
}

@end
