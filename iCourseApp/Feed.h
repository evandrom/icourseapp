//
//  Feed.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/10/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Person;

@interface Feed : NSManagedObject

@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) Person *whoPost;

@end
