//
//  ErrorHandler.h
//  iCurso
//
//  Created by Evandro Martinelli on 10/18/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ErrorHandler : NSObject

+ (void) showAlertWithString:(NSString *) text;

@end
