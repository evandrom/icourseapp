//
//  DashboardViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/1/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "DashboardViewController.h"
#import "FeedViewController.h"
#import "ViewController.h"
#import "UtilsViewController.h"
#import "Session.h"
#import "ProgressViewController.h"
#import "PersonService.h"

@interface DashboardViewController ()

@property (weak, nonatomic) IBOutlet UILabel *UInome;
@property (weak, nonatomic) IBOutlet UILabel *UIsenha;
@property (weak, nonatomic) IBOutlet UILabel *UIdatehour;


@end

@implementation DashboardViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    self.title = @"DashBoard";
    
    self.navigationItem.hidesBackButton = YES;
    
    [self setUIObjects];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.UIdatehour.text = [NSString stringWithFormat:@"Data: %@", [dateFormatter stringFromDate:[NSDate date]]];
}

- (void) setUIObjects {
    self.UInome.text = [NSString stringWithFormat:@"%@ %@", self.UInome.text, self.person.login];
    self.UIsenha.text = [NSString stringWithFormat:@"%@ %@", self.UIsenha.text, self.person.senha];
}

- (IBAction)FeedClick:(id)sender {
    //[self.person setFeedArray];
    
    FeedViewController *feedViewController = [FeedViewController new];
   // feedViewController.dataFeedArray = self.person.feedArray;
    [self.navigationController pushViewController:feedViewController animated:YES];
}

- (void) setModalWithPerson:(Person *) person{
    if (person != nil) {
        self.person = person;
    } else {
        Session *session = [Session sharedSession];
        self.person = session.person;
    }
}

- (IBAction)UtilsClick:(id)sender {
    UtilsViewController *utilsViewController = [UtilsViewController new];
    
    [self.navigationController pushViewController:utilsViewController animated:YES];
}

- (IBAction)ProgressTouch:(id)sender {
    ProgressViewController *progressViewController = [ProgressViewController new];
    
    [self.navigationController pushViewController:progressViewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
