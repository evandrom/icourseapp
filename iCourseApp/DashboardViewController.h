//
//  DashboardViewController.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/1/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface DashboardViewController : UIViewController

@property(nonatomic,strong) Person *person;

- (void) setModalWithPerson:(Person *) person;

@end
