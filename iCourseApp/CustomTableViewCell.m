//
//  CustomTableViewCell.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/8/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "CustomTableViewCell.h"

@interface CustomTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *UIImage;
@property (weak, nonatomic) IBOutlet UILabel *UIText;

@end

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setCellWithText:(NSString *) text andImage: (NSString *) image {
    self.UIText.text = text;
    //self.UIImage.image = [UIImage imageNamed:image];
}

@end
