//
//  ErrorHandler.m
//  iCurso
//
//  Created by Evandro Martinelli on 10/18/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "ErrorHandler.h"

@implementation ErrorHandler

+ (void) showAlertWithString:(NSString *) text {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção" message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert show];
}

@end
