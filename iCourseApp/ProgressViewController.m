//
//  ProgressViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 12/6/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "ProgressViewController.h"
#import "ProgressView.h"

@interface ProgressViewController ()

@property (strong, nonatomic) IBOutlet ProgressView *mainView;

@property (nonatomic, strong) NSOperationQueue *queue;

@property (nonatomic, strong) NSArray *operations;

@end

@implementation ProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Progress";
    [self.mainView setupProgressView];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (ProgressView *)mainView
{
    ProgressView *mainView = (ProgressView *) self.view;
    return mainView;
}

- (IBAction)startTouch:(id)sender {
    UIButton *button = (UIButton *) sender;
    button.enabled = NO;
    
    [self.queue cancelAllOperations];
    
    [self.mainView resetAllProgress];
    
    [self createOperation];
}

- (NSOperationQueue *) queue
{
    if(!_queue){
        _queue = [NSOperationQueue new];
    }
    return  _queue;
}

- (NSBlockOperation *) addOperationWithProgressView:(UIProgressView *) progress andFinalCount:(NSInteger) count andTimeInterval:(CGFloat) interval
{
    NSBlockOperation *operation = [NSBlockOperation new];
    
    __weak NSBlockOperation *weakOperation = operation;
    
    [operation addExecutionBlock:^{
        for (int i = 1; i <= count; i++) {
            int random = arc4random() % 15;
            
            if (random == 4) {
                [weakOperation cancel];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.mainView cancelProgress:progress];
                }];
                break;
            }
            
            [NSThread sleepForTimeInterval:interval];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.mainView setProgress:progress withInterval:((CGFloat) i/count)];
            }];
        }
    }];
    
    return operation;
}

- (void) createOperation
{
    NSBlockOperation *operation1 = [self addOperationWithProgressView:self.mainView.Progress1 andFinalCount:12 andTimeInterval:1.2];
    
    NSBlockOperation *operation2 = [self addOperationWithProgressView:self.mainView.Progress2 andFinalCount:20 andTimeInterval:0.8];

    NSBlockOperation *operation3 = [self addOperationWithProgressView:self.mainView.Progress3 andFinalCount:27 andTimeInterval:1.5];

    NSBlockOperation *operation4 = [self addOperationWithProgressView:self.mainView.Progress4 andFinalCount:8 andTimeInterval:2.0];
    
    NSBlockOperation *lastOperation = [NSBlockOperation new];
    [lastOperation addExecutionBlock:^{
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Done" delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil];
            
            self.mainView.Button1.enabled = YES;
            
            [alertView show];
        }];
    }];
    
    self.operations = @[operation1, operation2, operation3, operation4, lastOperation];
    
    self.queue.maxConcurrentOperationCount = 4;
    
    [lastOperation addDependency:operation1];
    [lastOperation addDependency:operation2];
    [lastOperation addDependency:operation3];
    [lastOperation addDependency:operation4];
    
    operation3.queuePriority = NSOperationQueuePriorityHigh;
    operation2.queuePriority = NSOperationQueuePriorityLow;
    
    [self.queue addOperations:self.operations waitUntilFinished:NO];
}

@end
