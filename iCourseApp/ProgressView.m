//
//  ProgressView.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 12/6/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "ProgressView.h"

@interface ProgressView()

@property (strong, nonatomic) IBOutletCollection(UIProgressView) NSArray *progressCollection;

@end

@implementation ProgressView

- (void)setupProgressView
{
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 10.0f);
    
    for (UIProgressView *progressView in self.progressCollection) {
        progressView.transform = transform;
        progressView.trackTintColor = [UIColor grayColor];
        progressView.progress = 0;
    }
    
}

- (void) cancelProgress:(UIProgressView *)progress
{
    progress.progress = 0;
    progress.trackTintColor = [UIColor redColor];
}

- (void)setProgress:(UIProgressView *)progress withInterval:(CGFloat)interval
{
    progress.progress = interval;
}

- (void)resetAllProgress
{
    for (UIProgressView *progressView in self.progressCollection) {
        progressView.progress = 0;
        progressView.trackTintColor = [UIColor grayColor];
    }
}

@end
