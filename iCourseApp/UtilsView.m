//
//  UtilsView.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/29/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "UtilsView.h"
#import "ProfileView.h"
#import "UIView+CustomView.h"

@interface UtilsView()

@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (nonatomic, strong) ProfileView *profileView;

@end

@implementation UtilsView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setup {
    self.backgroundColor = [UIColor greenColor];
}

- (void)setLabelValue:(NSString *)value {
    self.labelText.text = value;
}

- (void)addImageProfile:(id<ProfileDelegate>)delegate {
    CGRect frame = CGRectMake((self.frame.size.width / 2) - 50, (self.frame.size.height / 2) - 50, 100, 100);
     self.profileView = [[ProfileView alloc] initWithFrame:frame];
    
    self.profileView.delegate = delegate;
    
    [self.profileView cornerRadius:50];
    [self.profileView addBorder:3 andColor:[UIColor blackColor]];
    
    [self addSubview:self.profileView];
}

- (void)updateProfilePosition:(CGPoint )location {
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.profileView.center = location;
        
    } completion:^(BOOL finished) {
    }];
}


@end
