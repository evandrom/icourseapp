//
//  Person.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/10/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Feed;

@interface Person : NSManagedObject

@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * senha;
@property (nonatomic, retain) NSSet *feeds;
@end

@interface Person (CoreDataGeneratedAccessors)

- (void)addFeedsObject:(Feed *)value;
- (void)removeFeedsObject:(Feed *)value;
- (void)addFeeds:(NSSet *)values;
- (void)removeFeeds:(NSSet *)values;

@end
