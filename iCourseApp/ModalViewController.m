//
//  ModalViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/8/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *UIImage;
@property (weak, nonatomic) IBOutlet UILabel *UiText;
@property (weak, nonatomic) IBOutlet UIButton *UIDismiss;

@end

@implementation ModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Detalhes";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)DismissClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) setModalWithTitle: (NSString *) title andImage: (NSString *) image {
    self.UIImage.image = [UIImage imageNamed:image];
    self.UiText.text = title;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
