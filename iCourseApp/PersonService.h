//
//  PersonService.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 12/13/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface PersonService : NSObject

+ (void) feedFromPerson:(Person *)person withCompletationBlock:(void (^) (BOOL success, NSError *error)) completion;

@end
