//
//  MainViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/17/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"

@interface MainViewController ()

@property (strong, nonatomic) UINavigationController *mainNavigationController;
@property (assign, nonatomic) BOOL isMenuOpen;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    self.mainNavigationController = navigationController;
    self.mainNavigationController.view.bounds = self.view.bounds;
    
    [self.view addSubview:self.mainNavigationController.view];
    [self addChildViewController:self.mainNavigationController];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(handleMenu:)];
    
    viewController.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void) handleMenu: (UIBarButtonItem *) button {
    
    __block CGRect rect = self.mainNavigationController.view.frame;
        
        [UIView animateWithDuration:0.3 animations:^{
            if (self.isMenuOpen) {
                self.isMenuOpen = NO;
                rect.origin.x = 0.0f;
                self.mainNavigationController.view.frame = rect;
            } else {
                self.isMenuOpen = YES;
                rect.origin.x = 200.0f;
                self.mainNavigationController.view.frame = rect;
            }
        }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
