//
//  FeedViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/8/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "FeedViewController.h"
#import "Feed.h"
#import "CustomTableViewCell.h"
#import "ModalViewController.h"
#import "PersonService.h"
#import "Person.h"
#import "Feed+Accessor.h"
#import "Session.h"
#import "Person+Accessor.h"
#import "AppDelegate.h"

@interface FeedViewController ()

@property (weak, nonatomic) IBOutlet UITableView *UITableCell;

@property (nonatomic, strong) Person *person;

@property (nonatomic, strong) NSArray *result;

@end

@implementation FeedViewController

- (void)viewDidLoad {
// Do any additional setup after loading the view from its nib.

    [super viewDidLoad];
    self.title = @"Feed";
    UINib *UICustomCell = [UINib nibWithNibName:@"CustomTableViewCell" bundle:nil] ;
    [self.UITableCell registerNib:UICustomCell forCellReuseIdentifier:@"Cell"];
    
    int test = 10;
    
    void (^testBlock)(void) = ^{
        NSLog(@"Test = %d", test);
    };
    
    test = 20;
    
    testBlock();
    
    NSLog(@"Fora do Block = %d", test);
    
     //self.person = [Person new];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    Session *session = [Session sharedSession];
    
    self.person = [Person InsertNewObjectInManagedContext:appDelegate.managedObjectContext withLogin:session.person.login andPassword:session.person.senha];
    
    [PersonService feedFromPerson:self.person withCompletationBlock:^(BOOL success, NSError *error) {
        if (success) {
            self.result = [Feed retrieveFeedsFromPerson:self.person];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.UITableCell reloadData];
            });
        }
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [self doSomethingWithBlock:^(NSInteger value) {
//        NSLog(@"Result block: %ld", value);
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Delegate TableCell

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.result.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [tableView
//                             dequeueReusableCellWithIdentifier:@"Cell"];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
//    }
//
    CustomTableViewCell *cell = (CustomTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Feed *feed = self.result[indexPath.row];
    
    [cell setCellWithText:feed.titulo andImage:feed.image];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Feed *feed = self.dataFeedArray[indexPath.row];
    ModalViewController *modalViewController = [ModalViewController new];
    [modalViewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:modalViewController animated:YES completion:^{
        [modalViewController setModalWithTitle:feed.titulo andImage:feed.image];
    }];
}

#pragma mark - methods with block

- (void) doSomethingWithBlock:(void (^)(NSInteger value)) completionBlock
{
    NSLog(@"Block");
    
    NSInteger value = 10;
    
    completionBlock(value);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        [NSThread sleepForTimeInterval:5.0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Finish" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
        });
    });
}



@end
