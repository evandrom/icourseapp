//
//  UIView+CustomView.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/29/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CustomView)

- (void) cornerRadius:(CGFloat) radius;

- (void) addBorder:(CGFloat) width andColor:(UIColor*) color;

@end
