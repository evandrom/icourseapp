//
//  Session.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 12/6/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "Session.h"

@implementation Session

+ (instancetype)sharedSession
{
    static id sharedInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [Session new];
    });
    
    return sharedInstance;
}

@end
