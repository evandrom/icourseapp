//
//  UtilsView.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/29/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileView.h"

@interface UtilsView : UIView

- (void) setup;

- (void) setLabelValue:(NSString *) value;

- (void) addImageProfile:(id<ProfileDelegate>) delegate;

-(void) updateProfilePosition: (CGPoint)location;

@end
