//
//  ProfileView.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/29/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileDelegate <NSObject>

- (void) profileDidSelected;

@end

@interface ProfileView : UIView

@property (nonatomic, assign) id<ProfileDelegate> delegate;

@end
