//
//  ViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/1/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "ViewController.h"
#import "ErrorHandler.h"
#import "DashboardViewController.h"
#import "Person.h"
#import "Session.h"
#import "Person+Accessor.h"
#import "AppDelegate.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end

@implementation ViewController

- (void)viewDidLoad {
    self.title = @"Login";
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)Login:(id)sender {
    if (self.login.text.length == 0 || self.password.text.length == 0) {
        [ErrorHandler showAlertWithString:@"Login ou Senha em Branco. Favor Verificar"];
    } else {
        DashboardViewController *dashboardViewController = [DashboardViewController new];
        
        AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
        
        Person *person;
        
        person = [Person InsertNewObjectInManagedContext:appdelegate.managedObjectContext withLogin:self.login.text andPassword:self.password.text];
        
        Session *session = [Session sharedSession];
        session.person = person;
        person = nil;
        
        [dashboardViewController setModalWithPerson:person];
        
        [self.navigationController pushViewController:dashboardViewController animated:YES];
    }
}
- (IBAction)LoginWithStoryBoard:(id)sender {

    if (self.login.text.length == 0 || self.password.text.length == 0) {
        [ErrorHandler showAlertWithString:@"Login ou Senha em Branco. Favor Verificar"];
    } else {
      Person *person = [Person new];
      person.login = self.login.text;
      person.senha = self.password.text;
        
      [self performSegueWithIdentifier:@"dashboardsegue" sender:person];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"dashboardsegue"]) {
        DashboardViewController *dashboardViewController = segue.destinationViewController;
        dashboardViewController.person = sender;
    }
}

@end
