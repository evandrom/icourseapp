//
//  Session.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 12/6/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Session : NSObject

@property (nonatomic, strong) Person *person;

+ (instancetype) sharedSession;

@end
