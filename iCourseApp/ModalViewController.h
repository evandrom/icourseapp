//
//  ModalViewController.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/8/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewController : UIViewController

- (void) setModalWithTitle: (NSString *) title andImage: (NSString *) image;

@end
