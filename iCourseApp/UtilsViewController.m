//
//  UtilsViewController.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/29/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "UtilsViewController.h"
#import "UtilsView.h"
#import "ProfileView.h"

@interface UtilsViewController () <ProfileDelegate>

@property (strong, nonatomic) IBOutlet UtilsView *mainView;

@end

@implementation UtilsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Utils";
    
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *tapGesture  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    tapGesture.numberOfTapsRequired = 3;
    [self.mainView addGestureRecognizer:tapGesture];
}

- (void) handleGesture: (UITapGestureRecognizer *) tapGesture {
    
    CGPoint location = [tapGesture locationInView:self.mainView];
    
    [self.mainView updateProfilePosition:location];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UtilsView *)mainView
{
    UtilsView *mainView = (UtilsView *) self.view;
    return mainView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.mainView setup];
    [self.mainView addImageProfile:self];
}

#pragma mark - ProfileDelegate

- (void)profileDidSelected {
    [self.mainView setLabelValue:@"Toquei no Profile"];
}

@end
