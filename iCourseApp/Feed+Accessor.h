//
//  Feed+Accessor.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/10/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import "Feed.h"
#import "Person.h"

@interface Feed (Accessor)

- (instancetype) initFeedWithTitle: (NSString *) title andImage: (NSString *) image;

+ (instancetype) InsertNewObjectInManagedContext : (NSManagedObjectContext *) managedObjectContext;
+ (NSString *) entityName;
+ (NSArray *) retrieveFeedsFromPerson: (Person *) person;
@end
