//
//  UIView+CustomView.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/29/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import "UIView+CustomView.h"

@implementation UIView (CustomView)

- (void)cornerRadius:(CGFloat)radius {
    self.layer.cornerRadius = radius;
    [self.layer masksToBounds];
}

- (void)addBorder:(CGFloat)width andColor:(UIColor *)color {
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = width;
}

@end
