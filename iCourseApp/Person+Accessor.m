//
//  Person+Accessor.m
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/10/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import "Person+Accessor.h"
#import "Feed.h"
#import "Feed+Accessor.h"
#import "AppDelegate.h"


@implementation Person (Accessor)

- (void) parseFeedFromArray:(NSDictionary *)jsonDic
{
    AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
    
    for (NSDictionary *dic in jsonDic) {
        Feed *feed = [Feed InsertNewObjectInManagedContext:appdelegate.managedObjectContext];
                      
        feed.titulo = dic[@"feedTitle"];
        feed.whoPost = self;
        
    }
    
    [appdelegate SaveContext];
}

+ (NSString *) entityName {
    return @"Person";
}

+ (instancetype)InsertNewObjectInManagedContext:(NSManagedObjectContext *)managedObjectContext withLogin:(NSString *)login andPassword:(NSString *)senha {
    
    Person *person;
    
    NSFetchRequest *request = [NSFetchRequest new];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:[Person entityName] inManagedObjectContext:managedObjectContext];
    
    [request setEntity:entityDescription];
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"login == %@", login];
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"senha == %@", senha];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1,predicate2]];
    
    request.predicate = compoundPredicate;
    
    NSError *error = nil;
    NSArray *result = [managedObjectContext executeFetchRequest:request error:&error];
    
    if (result == nil) {
        NSLog(@"%@", error.description);
    }
    else if (result.count == 0) {
        person = [NSEntityDescription insertNewObjectForEntityForName:[Person entityName] inManagedObjectContext:managedObjectContext];
        
        person.login = login;
        person.senha = senha;
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        [appDelegate SaveContext];
        
    }
    else {
        person = [result lastObject];
    }
    
    return person;
    
}

@end
