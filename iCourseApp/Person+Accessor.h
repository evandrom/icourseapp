//
//  Person+Accessor.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 1/10/15.
//  Copyright (c) 2015 Evandro Martinelli. All rights reserved.
//

#import "Person.h"

@interface Person (Accessor)

- (void) parseFeedFromArray:(NSDictionary *)jsonDic;

+ (instancetype) InsertNewObjectInManagedContext : (NSManagedObjectContext *) managedObjectContext withLogin: (NSString *) login andPassword: (NSString *) senha;

+ (NSString *) entityName;

@end
