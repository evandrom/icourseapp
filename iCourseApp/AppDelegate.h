//
//  AppDelegate.h
//  iCourseApp
//
//  Created by Evandro Martinelli on 11/1/14.
//  Copyright (c) 2014 Evandro Martinelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic, readonly) NSManagedObjectModel *managedObjectModel;

@property (strong, nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void) SaveContext;

@end

